<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class System extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'code', 'description','vendor_id'];

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        self::creating(function ($system) {
            $system->code = uniqid();
        });
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_systems');

    }

    public function getLinkAttribute()
    {
        return link_to(route('systems.show', [$this->id]), $this->name);
    }
}
