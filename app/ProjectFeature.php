<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectFeature extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'description', 'code', 'start_date', 'end_date', 'id','project_id'
    ];
}
