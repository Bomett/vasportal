<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectUpdate;
use App\ProjectUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myProjects()
    {
        $page_name = "My Projects";
        $projects = Auth::user()->projects()->paginate(10);
        return view('projects.index', compact('page_name', 'projects'));
    }

    public function index()
    {
        $page_name = "Projects";
        $projects = Project::paginate(10);
        return view('projects.index', compact('page_name', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:projects,name',
            'description' => 'sometimes',
        ]);

        Project::updateOrCreate([
            'name' => $request->name,
            'start_date' => Carbon::now(),
            'description' => $request->description
        ]);
        $request->session()->flash('success', 'Project created successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $page_name = $project->name . ' details';
        return view('projects.show', compact('project', 'page_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Project $project)
    {
        $this->validate($request, [
            'name' => ['required',
                Rule::unique('projects')->ignore($project)],
            'description' => 'sometimes',
        ]);

        Project::updateOrCreate(
            [
                'id' => $project->id
            ],
            [
                'name' => $request->name,
                'description' => $request->description
            ]);
        $request->session()->flash('success', 'Project updated successfully');
        return back();
    }

    /**
     * @param Request $request
     * @param $projectId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveUpdate(Request $request, $projectId)
    {
        $project = Project::findOrFail($projectId);
        $this->validate($request, [
            'title' => 'required',
            'completion_level' => 'required',
            'description' => 'required',
        ]);
        ProjectUpdate::updateOrCreate([
            'project_id' => $project->id,
            'user_id' => $request->user()->id,
            'project_user_id' => $request->user()->id,
            'update_subject' => 'None',
            'completion_level' => $request->completion_level,
            'title' => $request->title,
            'description' => $request->description
        ]);
        $request->session()->flash('success', 'Project update save');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Project $project)
    {
        $project->delete();
        $request->session()->flash("success", "Project deleted");
        return route("projects.index");
    }


    public function joinProject(Request $request, $user_id, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $user = User::findOrFail($user_id);
        ProjectUser::updateOrCreate(
            [
                'user_id' => $user_id,
                'project_id' => $project_id
            ], [
                'role' => 'Member',
                'role_description' => 'Test'
            ]
        );

        $request->session()->flash('succcess', strtoupper($user->link) . " joined project " . strtoupper($project->link));
        return back();
    }

    public function leaveProject(Request $request, $user_id, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $user = User::findOrFail($user_id);
        ProjectUser::where(
            [
                'user_id' => $user_id,
                'project_id' => $project_id
            ]
        )->delete();

        $request->session()->flash('succcess', strtoupper($user->link) . " left project " . strtoupper($project->link));
        return back();
    }
}
