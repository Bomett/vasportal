<?php

namespace App\Http\Controllers;

use App\TeamBoard;
use Illuminate\Http\Request;

class TeamBoardController extends Controller
{
    public function index()
    {
        $page_name = 'Team Board';
        $posts = TeamBoard::orderBy('created_at','DESC')->paginate(20);
        return view('board.index', compact('page_name', 'posts'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_post' => 'required'
        ]);

        TeamBoard::create([
            'user_id' => $request->user()->id,
            'post' => $request->user_post
        ]);

        return back();
    }

    public function update(Request $request)
    {

    }

    public function destroy(Request $request)
    {

    }
}
