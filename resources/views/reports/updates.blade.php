@extends('layouts.app')
@section('content')
    <div class="x_panel">
        {{Form::open(['route' => ['reports.updates'],'method' => 'get'])}}
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <label>Users</label>
                <select class="form-control" name="users[]" multiple>
                    <option value="*" selected>All</option>
                    @foreach(\App\User::all() as $user)
                        <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Projects</label>
                <select class="form-control" name="projects[]" multiple>
                    <option value="*" selected>All</option>
                    @foreach(\App\Project::all() as $project)
                        <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Date range</label>
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                            class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="dates" id="reservation" class="form-control"
                                       value="01/01/2016 - 01/25/2016"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-success">Generate</button>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="x_panel">
        <div class="table-responsive">
            <table id="datatable-buttons" class="table  table-striped table-sm table-bordered">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Staff</th>
                    <th>Project</th>
                    <th>Status</th>
                    <th>Update</th>
                </tr>
                </thead>
                <tbody>
                @foreach($updates as $update)
                    <tr>
                        <td>{!! $update->created_at->format("d-m-Y") !!}</td>
                        <td>{!! $update->user->name !!}</td>
                        <td>{!! $update->project->name !!}</td>
                        <td>{!! $update->project->status_badge !!}</td>
                        <td>
                            <strong>{!! $update->title !!}</strong>
                            <p>{!! $update->description !!}</p>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('styles')

    <!-- Datatables -->
    <link href="{!! asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
@stop
@section('scripts')
    <!-- bootstrap-progressbar -->
    <script src="{!! asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{!! asset('/vendors/moment/min/moment.min.js') !!}"></script>
    <script src="{!! '/vendors/bootstrap-daterangepicker/daterangepicker.js' !!}"></script>

    <!-- Datatables -->
    <script src="{!! asset('vendors/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/buttons.print.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}"></script>
    <script src="{!! asset('vendors/jszip/dist/jszip.min.js') !!}"></script>
    <script src="{!! asset('vendors/pdfmake/build/pdfmake.min.js') !!}"></script>
    <script src="{!! asset('vendors/pdfmake/build/vfs_fonts.js') !!}"></script>

@stop
