@extends('layouts.app')

@section('content')
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
            <div class="count">{!! \App\User::count() !!}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>Total Projects</span>
            <div class="count">{!! \App\Project::count() !!}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>Ongoing Projects</span>
            <div class="count">{!! \App\Project::count() !!}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Updates this week</span>
            <div class="count">{!! \App\ProjectUpdate::where('created_at',">=", \Carbon\Carbon::now()->startOfWeek())->count() !!}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Core Systems</span>
            <div class="count">{!! \App\System::count() !!}</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recent Activities</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="dashboard-widget-content">
                        <ul class="list-unstyled timeline widget">
                            @foreach($updates as $update)
                                <li>
                                    <div class="block">
                                        <div class="block_content">
                                            <h2 class="title">
                                                <a>{!! $update->project->name !!}</a>
                                            </h2>
                                            <div class="byline">
                                                <span>{!! $update->created_at->diffForHumans() !!}</span> by
                                                <a>{!! $update->user->name !!}</a>
                                            </div>
                                            <p class="excerpt">{!! $update->description !!}</p>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Active Projects</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm  no-margin">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Project progress</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td>{!! $project->link !!}</td>
                                    <td>
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 data-transitiongoal="{!! $project->completion_level !!}"
                                                 aria-valuenow="49"
                                                 style="width: {!! $project->completion_level !!}%;"></div>
                                        </div>
                                    </td>
                                    <td>{!! $project->status_badge !!}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recent posts</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="dashboard-widget-content">
                        <ul class="list-unstyled timeline widget">
                            @foreach($posts as $post)
                                <li>
                                    <div class="block">
                                        <div class="block_content">
                                            <p class="excerpt">{!! $post->post !!}</p>
                                            <div class="byline">
                                                <span>{!! $post->created_at->diffForHumans() !!}</span> by
                                                <a>{!! $post->user->name !!}</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{!! asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
@stop