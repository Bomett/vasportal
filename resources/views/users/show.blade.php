@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{!! $user->name !!}
                        <small>Profile details</small>
                    </h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <h3>{!! $user->name !!}</h3>

                        <ul class="list-unstyled user_data">
                            <li><i class="fa fa-envelope user-profile-icon"></i>
                                {!! $user->email !!}
                            </li>

                            <li>
                                <i class="fa fa-briefcase user-profile-icon"></i> VAS Engineer
                            </li>
                        </ul>
                        @if(Gate::check('Edit users') || $user->is_me)
                            <a class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                        class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                        @endif
                        <br/>


                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                       aria-expanded="true">Recent activities</a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#tab_content2" role="tab" id="profile-tab"
                                       data-toggle="tab" aria-expanded="false">Projects</a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                     aria-labelledby="home-tab">

                                    <ul class="messages">
                                        @foreach($user->updates as $update)
                                            <li>
                                                <div class="message_date">
                                                    <h4 class="date text-info">{!! $update->completion_level !!}%</h4>
                                                    <p class="month">{!! $update->created_at->diffForHumans() !!}</p>
                                                </div>
                                                <div class="message_wrapper">

                                                    <h5><strong>{!! $update->title !!}</strong>
                                                        <small>
                                                            - {!! $update->user->link !!}
                                                        </small>
                                                    </h5>
                                                    <p class="message">
                                                        {!! $update->description !!}
                                                        <br/>
                                                        -
                                                        <small>{!! $update->project->link !!}</small>
                                                    </p>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                     aria-labelledby="profile-tab">

                                    <table class="table table-striped table-sm  no-margin">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Team Members</th>
                                            <th>Project progress</th>
                                            <th>Status</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($user->projects as $project)
                                            <tr>
                                                <td>{!! $project->id !!}</td>
                                                <td>{!! $project->link !!}</td>
                                                <td>{!! $project->members->count() !!}</td>
                                                <td>
                                                    <div class="progress progress_sm">
                                                        <div class="progress-bar bg-green" role="progressbar"
                                                             data-transitiongoal="{!! $project->completion_level !!}"
                                                             aria-valuenow="49"
                                                             style="width: {!! $project->completion_level !!}%;"></div>
                                                    </div>
                                                </td>
                                                <td>{!! $project->status_badge !!}</td>
                                                <td>{!! $project->description !!}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @if(Gate::check('Edit users') || $user->is_me)
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Edit {!! $user->name !!}</h4>
                    </div>
                    {{ Form::model($user,['route' => ['users.update', $user->id],'method' => 'put']) }}

                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>First Name<span class="required">*</span></label>
                                {!!  Form::text('first_name', $value = $user->first_name, $attributes =[
                                'class' => 'form-control'
                                ]); !!}
                            </div>
                            <div class="col-sm-6">
                                <label>Last Name<span class="required">*</span></label>
                                {!!  Form::text('last_name', $value = $user->last_name, $attributes =[
                                'class' => 'form-control'
                                ]); !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Role<span class="required">*</span></label>
                            <select class="form-control" name="role">
                                @foreach($roles as $role)
                                    <option {!! $user->role == $role->name ? 'selected':'' !!} value="{!! $role->id !!}">{!! $role->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Email address<span class="required">*</span></label>
                            {!!  Form::email('email', $value = $user->email, $attributes =[
                            'class' => 'form-control'
                            ]); !!}
                        </div>
                        @can('Reset user password')
                            @if(!$user->is_me)
                                <div class="form-group">
                                    <label>Set New Password<span class="required">*</span></label>
                                    {!!  Form::password( $value = null, $attributes =['name'=>'password',
                                    'class' => 'form-control'
                                    ]); !!}
                                </div>
                            @endif
                        @endcan
                        @if($user->is_me)
                            <div class="form-group">
                                <label>Reset Password<span class="required">*</span></label>
                                {!!  Form::password( $value = null, $attributes =['name'=>'password',
                                'class' => 'form-control'
                                ]); !!}
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endif
@stop

@section('styles')
    <link href="{!! asset('vendors/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
@stop
@section('scripts')
    <script src="{!! asset('/vendors/raphael/raphael.min.js') !!}"></script>
    <script src="{!! asset('/vendors/morris.js/morris.min.js') !!}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{!! asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{!! asset('/vendors/moment/min/moment.min.js') !!}"></script>
    <script src="{!! '/vendors/bootstrap-daterangepicker/daterangepicker.js' !!}"></script>

@stop
